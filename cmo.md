# Create CMO
## Event on Change Bulan
API Pertama
*Request*
```
fetch("http://localhost:1337/tanggalcmo?tahun=2021&bulan=12&week_number=1", {
  "headers": {
    "accept": "application/json",
    "accept-language": "id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7",
    "if-none-match": "W/\"34e-x8sZz6kUL1PbHJX8KCbPgtMPkr0\"",
    "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"Windows\"",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-site"
  },
  "referrer": "http://localhost:3000/",
  "referrerPolicy": "strict-origin-when-cross-origin",
  "body": null,
  "method": "GET",
  "mode": "cors",
  "credentials": "omit"
});
```

*Response*
```
{
  "status": 200,
  "error": false,
  "message": "Fetch data successfully",
  "results": [
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 1
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 2
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 3
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 4
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 5
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 6
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 7
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 8
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 9
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 10
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 11
    }
  ]
}
```
---
API Kedua
*Request*
```
fetch("http://localhost:1337/tanggalcmo?tahun=2021&bulan=12&week_number=2", {
  "headers": {
    "accept": "application/json",
    "accept-language": "id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7",
    "if-none-match": "W/\"243-0BqrdDh/NPpvZv5Go5xA+Hn2btU\"",
    "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"Windows\"",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-site"
  },
  "referrer": "http://localhost:3000/",
  "referrerPolicy": "strict-origin-when-cross-origin",
  "body": null,
  "method": "GET",
  "mode": "cors",
  "credentials": "omit"
});
```
*Response*
```
{
  "status": 200,
  "error": false,
  "message": "Fetch data successfully",
  "results": [
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 12
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 13
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 14
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 15
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 16
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 17
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 18
    }
  ]
}
```
---
API Ketiga
*Request*
```
fetch("http://localhost:1337/tanggalcmo?tahun=2021&bulan=12&week_number=3", {
  "headers": {
    "accept": "application/json",
    "accept-language": "id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7",
    "if-none-match": "W/\"243-/C/guR1yJQtPxTJW4UWoR0EyzsM\"",
    "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"Windows\"",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-site"
  },
  "referrer": "http://localhost:3000/",
  "referrerPolicy": "strict-origin-when-cross-origin",
  "body": null,
  "method": "GET",
  "mode": "cors",
  "credentials": "omit"
});
```
*Response*
```
{
  "status": 200,
  "error": false,
  "message": "Fetch data successfully",
  "results": [
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 19
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 20
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 21
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 22
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 23
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 24
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 25
    }
  ]
}
```
---
API Keempat
*Request*
```
fetch("http://localhost:1337/tanggalcmo?tahun=2021&bulan=12&week_number=4", {
  "headers": {
    "accept": "application/json",
    "accept-language": "id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7",
    "if-none-match": "W/\"1fe-SyqN5knx3ZPgCfCoWoqMdn9o1OA\"",
    "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"Windows\"",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-site"
  },
  "referrer": "http://localhost:3000/",
  "referrerPolicy": "strict-origin-when-cross-origin",
  "body": null,
  "method": "GET",
  "mode": "cors",
  "credentials": "omit"
});
```
*Response*
```
{
  "status": 200,
  "error": false,
  "message": "Fetch data successfully",
  "results": [
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 26
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 27
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 28
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 29
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 30
    },
    {
      "tahun": 2021,
      "bulan": 12,
      "tgl": 31
    }
  ]
}
```
---
API Kelima - Kemungkinan ini ditriger ketika on-change suatu value.
Response disini untuk isi field dengan placeholder *[Pilih Bulan, Tahun]*
*Request*
```
fetch("http://localhost:1337/documentnumber?kode=CMO&org_id=03180C15-E354-4C85-80ED-DF03EC627AA7&bulan=12&tahun=2021", {
  "headers": {
    "accept": "application/json",
    "accept-language": "id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7",
    "if-none-match": "W/\"77-OPVYiPKLU0GgGA9lS4PZ4vSJj5c\"",
    "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"Windows\"",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-site"
  },
  "referrer": "http://localhost:3000/",
  "referrerPolicy": "strict-origin-when-cross-origin",
  "body": null,
  "method": "GET",
  "mode": "cors",
  "credentials": "omit"
});
```
*Response*
```
{
  "status": 200,
  "error": false,
  "message": "Fetch data successfully",
  "result": "2021/CMO/DEC/1000017/00009"
}
```